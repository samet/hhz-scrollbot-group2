import scrollphathd 
import signal
import time
import threading 
import json
from scrollphathd.fonts import font5x7
from scrollphathd.fonts import font3x5
import paho.mqtt.client as paho 

str_len = 0
scroll_x = 0 

scrollphathd.set_brightness(0.5)
scrollphathd.rotate(180)
scrollphathd.clear()

def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

def on_subscribe(client, userdata, mid, granted_qos):
    str_len = scrollphathd.write_string("Connected", x = 0, y = 0, font=font3x5)
    scrollphathd.show()
    print("Subscribed: " +str(mid)+" "+ str(granted_qos))

def on_message(client, userdata, msg):
    scrollphathd.clear()
    message= msg.payload
    print_msg(msg)

def print_msg(msg):
    print(msg.payload)
    current_event = (msg.paload.decode()).split('event":"')[1].split('",')[0]
    msg.payload = get_msg(msg.payload.decode())´

    for i in range(8 * len(str(msg.payload))):
    #while check_event(current_event, msg.payload.decode()):
        str_len = scrollphathd.write_string(msg.payload, x = 0, y = 0, font=font5x7)
        scrollphathd.show()
        scrollphathd.scroll(1)
        time.sleep(0.02)
        i += 1

    scrollphathd.clear()
    scrollphathd.show()
    print(msg.topic+" " +str(msg.qos)+" "+str(msg.payload))

def check_event(current_event, msg):
    event = msg.split('event":"')[1].split('",')[0]
    if(current_event != event):
        on_message
        return False
    else:
        return True

def get_msg(msg):
    print(msg)
    event= msg.split('event":"')[1].split('",')[0]
    print(event)
    if(event == 'S'):
        return "Pause   "
    if(event == 'SS'):
        return "Vorlesung  "
    if(event == 'L'):
        return "Uebung   "
    else:
        return "Undefined   "


client = paho.Client()
client.on_connect = on_connect
client.on_subscribe = on_subscribe
client.on_message = on_message
client.connect("broker.hivemq.com", 1883)
client.subscribe("hhz/iot/ss22/#", qos=1) 
client.loop_forever()



